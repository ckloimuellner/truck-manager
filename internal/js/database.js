window.Database = function() {
  this.dbDeferred = $.Deferred();
  this.dbPromise = this.dbDeferred.promise();
  
  // In the following line, you should include the prefixes of implementations you want to test.
  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
  // DON'T use "var indexedDB = ..." if you're not in a function.
  // Moreover, you may need references to some window.IDB* objects:
  window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
  window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
  // (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
  
  if (!window.indexedDB) {
    throw "Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.";
  }
  
  // Let us open our database
  var request = window.indexedDB.open("TruckManagerDB", 1),
    self = this;
  request.onerror = function(ev) {
    throw "An error occurred while trying to open db " + JSON.stringify(ev);
  };
  request.onsuccess = function(ev) {
    self.db = request.result;
    self.dbDeferred.resolveWith(self, [self.db]);
  };
  
  // This event is only implemented in recent browsers
  request.onupgradeneeded = function(event) { 
    self.db = event.target.result;
    
    // Create an objectStore for this database
    var objectStore = self.db.createObjectStore("user", { keyPath: "id", autoIncrement: true });
    objectStore.createIndex("username", "username", {unique: true});
    objectStore.createIndex("email", "email",  {unique: false});
    
    var mockdata = [
      {
        "firstname": "Karyn",
        "lastname": "Atkinson",
        "companyname": "Sapien Incorporated",
        "country": "Faroe Islands",
        "phone": "(032196) 083108",
        "iban": "DK1137732061010347",
        "bic": "ZFK27NSO7UI",
        "username": "claudia",
        "email": "hendrerit.consectetuer@ligulaAliquam.com",
        "password": "IUL86ZYB1KH"
      },
      {
        "firstname": "Breanna",
        "lastname": "Wells",
        "companyname": "Malesuada Ut Ltd",
        "country": "Norfolk Island",
        "phone": "(073) 66366175",
        "iban": "MR3059634223052694272490615",
        "bic": "YTI79SFJ9SE",
        "username": "maria",
        "email": "pretium@loremsemperauctor.org",
        "password": "YXB20NFG2LI"
      },
      {
        "firstname": "Leilani",
        "lastname": "Jarvis",
        "companyname": "Auctor LLP",
        "country": "Andorra",
        "phone": "(033517) 210709",
        "iban": "MK30132561816497991",
        "bic": "WOJ42OEX8LX",
        "username": "julia",
        "email": "tincidunt.nunc@ut.net",
        "password": "DIU59GXF7NZ"
      },
      {
        "firstname": "Maggie",
        "lastname": "Fuentes",
        "companyname": "Etiam Ligula Corp.",
        "country": "Northern Mariana Islands",
        "phone": "(0216) 64063028",
        "iban": "VG4208897042640669792494",
        "bic": "HKX20VHY0AQ",
        "username": "lisa",
        "email": "in.sodales@tinciduntadipiscing.co.uk",
        "password": "LUC08AID8SM"
      },
      {
        "firstname": "Kelly",
        "lastname": "Mack",
        "companyname": "Proin Foundation",
        "country": "Pitcairn Islands",
        "phone": "(006) 31568409",
        "iban": "RS76777204841557203987",
        "bic": "OWI96JPC5YL",
        "username": "max",
        "email": "lectus.pede.et@convallisin.ca",
        "password": "GBH42WZW2XT"
      },
      {
        "firstname": "Isadora",
        "lastname": "Robles",
        "companyname": "Arcu Corp.",
        "country": "Saint Martin",
        "phone": "(01273) 8809171",
        "iban": "GB43KHSI52852033385415",
        "bic": "NEL73SWA9ZR",
        "username": "thorsten",
        "email": "odio@quam.net",
        "password": "LPN63WUT9IU"
      }
    ];
    
    for(var i in mockdata) {
      objectStore.put(mockdata[i]);
    }
    
    var truckOs = self.db.createObjectStore("truck", { keyPath: "id", autoIncrement: true });
    truckOs.createIndex("username", "username", {unique: false});
    
    var customerOs = self.db.createObjectStore("customer", { keyPath: "id", autoIncrement: true });
    customerOs.createIndex("username", "username", {unique: false});
    
    var goodsOs = self.db.createObjectStore("goods", { keyPath: "id", autoIncrement: true });
    goodsOs.createIndex("username", "username", {unique: false});
    
    var routeOs = self.db.createObjectStore("route", { keyPath: "id", autoIncrement: true });
    routeOs.createIndex("username", "username", {unique: false});
  };
};
  
window.Database.prototype.findUser = function(username, pwd) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["user"]).objectStore("user").index("username").get(username);
    request.onsuccess = function(ev) {
      if(ev.target.result.password === pwd) {
        thisDeferred.resolve(ev.target.result);
      } else {
        console.error("password incorrect");
        thisDeferred.reject();
      }
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      /*console.error(ev);
      throw "Error while retrieving user from database with username " + username;*/
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.addUser = function(user) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["user"], "readwrite").objectStore("user").put(user);
    request.onsuccess = function(ev) {
      thisDeferred.resolve(ev.target.result);
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error(ev, user);
      throw "Error while persisting new user into database " + JSON.stringify(user);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.getTrucksForUser = function(username) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var singleKeyRange = IDBKeyRange.only(username);
    var request = db.transaction(["truck"]).objectStore("truck").index("username").openCursor(singleKeyRange);
    var trucks =[];
    request.onsuccess = function(ev) {
      var cursor = ev.target.result;
      if(cursor) {
        trucks.push(cursor.value);
        cursor.continue();
      } else {
        thisDeferred.resolve(trucks);
      }
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error while getting trucks for user", username);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.getCustomersForUser = function(username) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var singleKeyRange = IDBKeyRange.only(username);
    var request = db.transaction(["customer"]).objectStore("customer").index("username").openCursor(singleKeyRange);
    var customers =[];
    request.onsuccess = function(ev) {
      var cursor = ev.target.result;
      if(cursor) {
        customers.push(cursor.value);
        cursor.continue();
      } else {
        thisDeferred.resolve(customers);
      }
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error while getting customers for user", username);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.getGoodsForUser = function(username) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var singleKeyRange = IDBKeyRange.only(username);
    var request = db.transaction(["goods"]).objectStore("goods").index("username").openCursor(singleKeyRange);
    var goods =[];
    request.onsuccess = function(ev) {
      var cursor = ev.target.result;
      if(cursor) {
        goods.push(cursor.value);
        cursor.continue();
      } else {
        thisDeferred.resolve(goods);
      }
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error while getting goods for user", username);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.addTruck = function(truck) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["truck"], "readwrite").objectStore("truck").add(truck);
    request.onsuccess = function(ev) {
      truck.id = ev.target.result;
      thisDeferred.resolve(truck);
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error occured while adding truck to database", truck, ev);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.addCustomer = function(customer) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["customer"], "readwrite").objectStore("customer").add(customer);
    request.onsuccess = function(ev) {
      customer.id = ev.target.result;
      thisDeferred.resolve(customer);
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error occured while adding customer to database", customer, ev);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.addGood = function(good) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["goods"], "readwrite").objectStore("goods").add(good);
    request.onsuccess = function(ev) {
      good.id = ev.target.result;
      thisDeferred.resolve(good);
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error occured while adding good to database", good, ev);
    };
  });
  
  return thisDeferred.promise();
};

window.Database.prototype.addRoute = function(route) {
  var thisDeferred = $.Deferred();
  this.dbPromise.done(function(db) {
    var request = db.transaction(["route"], "readwrite").objectStore("route").add(route);
    request.onsuccess = function(ev) {
      route.id = ev.target.result;
      thisDeferred.resolve(route);
    };
    request.onerror = function(ev) {
      thisDeferred.reject();
      console.error("error occured while adding route to database", route, ev);
    };
  });
  
  return thisDeferred.promise();
};
  
window.db = new window.Database();