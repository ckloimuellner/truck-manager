(function() {
  console.log($("#register-form").validate({
    rules: {
      "firstname-input": "required",
      "lastname-input": "required",
      "compnanyname-input": "required",
      "username-input": {
        required: true,
        maxlength: 50
      },
      "pwd-input": {
        required: true,
        maxlength: 50
      },
      "email-input": {
        required: true,
        maxlength: 80
      }
    },
    errorPlacement: function($error, $el) {
      $error.appendTo( $el.parent().next() );
    }
  }));//.valid();
  
  $("#register-form").valid();
  
  $("#register-btn").on("click", function(ev) {
    ev.preventDefault();
    
    if($("#register-form").valid()) {
      var user = {
        firstname: $("#firstname-input").val(),
        lastname: $("#lastname-input").val(),
        companyname: $("#companyname-input").val(),
        address: $("#address-ta").val(),
        country: $("#country-select").val(),
        phone: $("#phone-input").val(),
        iban: $("#iban-input").val(),
        bic: $("#bic-input").val(),
        username: $("#username-input").val(),
        email: $("username-reg-input").val(),
        password: $("#pwd-reg-input").val()
      };
      
      window.db.addUser(user).done(function() {
        toastr.info("User account for " + user.username + " was created successfully!");
        $("input, textarea, select").each(function(i, el) {
          el.value = "";
        });
      });
    }
  });
  
})();