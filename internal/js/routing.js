(function() {
  var user = JSON.parse(sessionStorage.getItem("user"));
  window.currentStep = 1;
  window.mapOptions = {
    zoom:7,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: new google.maps.LatLng(48.108659, 15.539328)
  };
  window.directionsService = new google.maps.DirectionsService();
  
  $.when(window.db.getTrucksForUser(user.username),
         window.db.getCustomersForUser(user.username),
         window.db.getGoodsForUser(user.username)
        ).then(function(trucks, customers, goods) {
           var $trucksUL = $(".truck-box ul"),
               $customersUL = $(".customer-box ul");
           $.each(trucks, function(i, truck) {
             $trucksUL.append("<li data-vehicle='" + JSON.stringify(truck) + "'>" + truck.name + "</li>");
           });
           $.each(customers, function(i, customer) {
             $customersUL.append("<li data-customer='" + JSON.stringify(customer) + "'>" + customer.name + "</li>");
           });
           $.each(goods, function(i, good) {
             $("#initial-goods-form fieldset").prepend(
               "<div class='spinner-wrapper'> \
                 <div class='fe'><label>" + good.name + "</label></div> \
                 <div class='fe'><input data-id='" + good.id + "' data-name='" + good.name + "' class='spinner' value='0'></div> \
               </div>"
                                              );
           });
         });
         
  $(".wrapper-box ul").on("click", "li", function(ev) {
    var $el = $(ev.target);
    if($el.hasClass("selected")) {
      $el.removeClass("selected");
    } else {
      $el.addClass("selected");
    }
  });
  
  $(".select-all").on("click", "a", function(ev) {
    $(ev.target).closest(".box").find("li").addClass("selected");
  });
  
  $(".main-content").on("click", ".next", function(ev) {
    var $delegateTarget = $(ev.delegateTarget);
    $delegateTarget.find(".step" + window.currentStep).fadeOut("fast", function() {
      window.currentStep = window.currentStep + 1;
      $delegateTarget.find(".step" + window.currentStep).fadeIn("fast");
      
      if(window.currentStep === 2) {
        var $vehicleTable = $("#vehicle-table tbody").empty(),
            $customerTable = $("#customer-table tbody").empty();                         
          $(".truck-box .selected").each(function(i, e) {
            var sVehicle = e.getAttribute("data-vehicle"),
                oVehicle = JSON.parse(e.getAttribute("data-vehicle"));
            $("<tr data-vehicle='" + sVehicle + "'> \
            <td>" + oVehicle.name + "</td>\
            <td>" + oVehicle.capacity + "</td> \
            <td><a href='javascript:void(0)' class='initial-goods-lnk'>Select</a></td> \
            <td><a href='javascript:void(0)' class='location-picker-lnk'>Select</a></td> \
            </tr>").appendTo($vehicleTable);
          });
          
          $(".customer-box .selected").each(function(i, e) {
            var sCustomer = e.getAttribute("data-customer"),
                oCustomer = JSON.parse(sCustomer);
            $("<tr data-customer='" + sCustomer + "'> \
              <td>" + oCustomer.name + "</td>\
              <td>" + oCustomer.location + "</td>\
              <td><a href='javascript:void(0)' class='initial-goods-lnk'>Select</a></td> \
              </tr>").appendTo($customerTable);
          });
      }
      if(window.currentStep === 3) {
        var vehicles = [],
            customers = [];
        $("#vehicle-table tbody tr").each(function(i, e) {
          var vehicle = JSON.parse(e.getAttribute("data-vehicle")),
              goods = e.getAttribute("data-goods"),
              location = e.getAttribute("data-loc");
          
          vehicles.push({
            id: vehicle.id,
            name: vehicle.name,
            goods: JSON.parse(goods),
            location: JSON.parse(location)
          });
        });
        
        $("#customer-table tbody tr").each(function(i, e) {
          var customer = JSON.parse(e.getAttribute("data-customer")),
              goods = e.getAttribute("data-goods");
              
           customers.push({
             id: customer.id,
             name: customer.name,
             goods: JSON.parse(goods),
             lat: customer.lat,
             lng: customer.lng
           });
        });
        var vehiclePlaces = $.map(vehicles, function(v, i) {
            return v.location.lat + "," + v.location.lng;
          }),
          customerPlaces = $.map(customers, function(c, i) {
            return c.lat + "," + c.lng;
          }),
          cities = vehiclePlaces.concat(customerPlaces).join("|"),
          params = [
            {name: "origins", value: cities},
            {name: "destinations", value: cities}
          ],
          distancePromise = $.ajax({
            type: "GET",
            url: "http://maps.googleapis.com/maps/api/distancematrix/json",
            data: params
          }),
          dbPromise = window.db.addRoute((window.currentRoute={
            username: user.username,
            vehicles: vehicles,
            customers: customers
          }));
        $.when(distancePromise, dbPromise).then(function(dist, route) {
          console.log("all operations succeeded successfully!", dist, route);
          
          window.directionsDisplay = [];
          window.directionsResult = {};
          window.directionsMaps = [];
          var deferred = $.Deferred(), i = 0;
          $.each(route.vehicles, function(i, v) {
            var customer = route.customers[Math.floor((Math.random() * route.customers.length))],
                 request = {
                   origin: new google.maps.LatLng(v.location.lat, v.location.lng), 
                   destination: new google.maps.LatLng(customer.lat, customer.lng),
                   travelMode: google.maps.TravelMode.DRIVING
                 };
            window.directionsService.route(request, function(result, status) {
              if(status == google.maps.DirectionsStatus.OK) {
                window.directionsResult[v.id] = result;
              }
              i = i + 1;
              console.log("i is now", i, "all vehicles are", route.vehicles.length);
              if(i === route.vehicles.length) {
                console.log("resolving");
                deferred.resolve();
              }
            });
          });
          
          deferred.promise().done(function() {
            console.log("deferred called, generating routes");
            $.each(route.vehicles, function(i, v) {
              $("#accordion").append("<h3>" + v.name + "</h3> \
                  <div data-id='" + v.id + "'> \
                    <div class='gmap'></div> \
                    <div><p><a href='javascript:void(0)'>Send route to driver</a></p></div>\
                  </div>");
            });
            var doMaps = function(header, panel) {
              console.log("domaps called");
              if(!header.size() && !panel.size()) {
                return;
              }
              
              var vid = panel.attr("data-id");
              if(!window.directionsMaps[vid]) {
                window.directionsMaps[vid] = new google.maps.Map(panel.find(".gmap")[0], window.mapOptions);
                var directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setMap(window.directionsMaps[vid]);
                directionsDisplay.setDirections(window.directionsResult[vid]);
              }
            };
            $("#accordion").accordion({
              activate: function(event, ui) {
                doMaps(ui.newHeader, ui.newPanel);
              },
              create: function(event, ui) {
                doMaps(ui.header, ui.panel);
              }
            });
          });
        });
      }
    });
  });
  
  $(".main-content").on("click", ".previous", function(ev) {
    var $delegateTarget = $(ev.delegateTarget);
    $delegateTarget.find(".step" + window.currentStep).fadeOut("fast", function() {
      window.currentStep = window.currentStep - 1;
      $delegateTarget.find(".step" + window.currentStep).fadeIn("fast");
    });
  });
  
  $(".main-content").on("click", ".initial-goods-lnk", function(ev) {
    window.$activeElem = $(ev.target).closest("tr");
    $("#initial-goods-form").modal({
      persist: false,
      onShow: function(dialog) {
        var sGoods = window.$activeElem.attr("data-goods"), oGoods = {};
        if(sGoods) {
          oGoods = JSON.parse(sGoods);
        }
        $("input.spinner").each(function(i, el) {
          var id = el.getAttribute("data-id");
          
          $(el).spinner().spinner("value", oGoods[id] ? (parseInt(oGoods[id].value, 10) || 0) : 0);
        });
      }
    });
  });
  $(".main-content").on("click", ".location-picker-lnk", function(ev) {
    window.$activeElem = $(ev.target).closest("tr");
    $("#location-picker-mp").modal({
      onShow: function(dialog) {
        if(!window.gmap) {
          window.createGmap();
        }
        
        document.getElementById('pac-input').value = "";
        
        var sLoc = window.$activeElem.attr("data-loc");
        
        if(!sLoc) {
          window.removeMarkers();
          window.setInitialBounds();
        } else {
          var oLoc = JSON.parse(sLoc);
          window.createMarker(oLoc);
        }
      },
      persist: true,
      minHeight: "600px"
    });
  });
  
  $("#initial-goods-form").on("click", ".save", function(ev) {
    var oGoods = {};
    $(ev.delegateTarget).find("input.spinner").each(function(i, el) {
      oGoods[el.getAttribute("data-id")] = {
        value: $(el).spinner("value"),
        name: el.getAttribute("data-name")
      };
      window.$activeElem.find("td:eq(2)").html("<a href='javascript:void(0)' class='initial-goods-lnk'>Done</a>");
    });
    window.$activeElem.attr("data-goods", JSON.stringify(oGoods));
    $.modal.close();
  });
  
  $("#initial-goods-form").on("click", ".cancel", function(ev) {
    $.modal.close();
  });
  $("#location-picker-mp").on("click", ".save", function(ev) {
    window.$activeElem.find("td:eq(3)").html("<a href='javascript:void(0)' class='location-picker-lnk'>Done</a>");
    $.modal.close();
  });
  $("#location-picker-mp").on("click", ".cancel", function(ev) {
    $.modal.close();
  });
  
  // Google Maps Places
  window.gmap = null;
  window.markers = [];
  window.createGmap = function() {
    window.gmap = new google.maps.Map(document.getElementById('map-canvas'), {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 10
    });
    
    
    window.setInitialBounds();
    
    // Create the search box and link it to the UI element.
    var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
    window.gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
    var searchBox = new google.maps.places.SearchBox(
      /** @type {HTMLInputElement} */(input));
    
    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces(), place = places[0];
      
      if(!place) {
        return;
      }
      
      
      //for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
                                  origin: new google.maps.Point(0, 0),
                                  anchor: new google.maps.Point(17, 34),
                                  scaledSize: new google.maps.Size(25, 25)
      };
      
      console.log(place.geometry.location.lng(), place.geometry.location.lat())
      
      var loc = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
        title: place.name,
        image: image
      };
      
      window.$activeElem.attr("data-loc", JSON.stringify(loc));
      
      console.log("place", place);
      
      window.createMarker(loc);
     
    });
    // [END region_getplaces]
    
    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(window.gmap, 'bounds_changed', function() {
      var bounds = window.gmap.getBounds();
      searchBox.setBounds(bounds);
    });
  };
  
  
  window.createMarker = function(loc) {
    window.removeMarkers();
    
    // Create a marker for each place.
    var marker = new google.maps.Marker({
      map: window.gmap,
      icon: loc.image,
      draggable: true,
      title: loc.title /* place.name */,
      position: new google.maps.LatLng(loc.lat, loc.lng) /* place.geometry.location */
    });
    
    window.markers.push(marker);
    
    google.maps.event.addListener(marker, 'dragend', function(event) {
      console.log(event);
      console.debug('final position is '+event.latLng.lat()+' / '+event.latLng.lng());
      
      window.$activeElem
        .attr("data-loc", JSON.stringify({
          lat: event.latLng.lat(),
          lng: event.latLng.lng(),
          title: title,
          image: image
        })
      );
    });
    
    var bounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(loc.lat - 0.05, loc.lng - 0.05),
                                              new google.maps.LatLng(loc.lat + 0.05, loc.lng + 0.05)
    );
    //bounds.extend(place.geometry.location);
    //}
    
    window.gmap.fitBounds(bounds);
  };
  
  window.removeMarkers = function() {
    for (var i = 0, marker; marker = window.markers[i]; i++) {
      marker.setMap(null);
    }
    
    // For each place, get the icon, place name, and location.
    window.markers = [];
  };
  
  window.setInitialBounds = function() {
    var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(48.108659, 15.539328),
                                                     new google.maps.LatLng(48.308659, 15.739328));
    window.gmap.fitBounds(defaultBounds);
  };
})();