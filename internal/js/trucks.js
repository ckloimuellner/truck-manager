(function() {
  
  var user = JSON.parse(sessionStorage.getItem("user"));
  
  window.db.getTrucksForUser(user.username).done(function(trucks) {
    window.truckTable = $("#truck-table").DataTable({
      searching: false,
      lengthChange: false,
      data: trucks,
      columns: [
        {
          data: "name",
          title: "Name",
          type: "string",
          sortable: false
        },
        {
          data: "capacity",
          title: "Capacity",
          type: "number",
          sortable: false
        }
      ]
    });
  }).fail(function() {
    console.error("error while querying trucks");
    throw "truck query error";
  });
  
  $(".new-truck").on("click", function(ev) {
    $("#add-truck-mp").modal();
  });
  
  $("#save-vehicle").on("click", function(ev) {
    window.db.addTruck({
      name: $("#name-input").val(),
      capacity: $("#capacity-input").val(),
      username: JSON.parse(sessionStorage.getItem("user")).username
    }).done(function(truck) {
      $.modal.close();
      window.truckTable.row.add(truck).draw();
      toastr.success("Successfully added truck!");
    }).fail(function() {
      
    });
  });
  
  $("#cancel-mp").on("click", function(ev) {
    $.modal.close();
  });
})();