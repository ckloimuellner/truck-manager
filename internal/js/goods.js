(function() {
  
  var user = JSON.parse(sessionStorage.getItem("user"));
  
  window.db.getGoodsForUser(user.username).done(function(goods) {
    window.goodsTable = $("#goods-table").DataTable({
      searching: false,
      lengthChange: false,
      data: goods,
      columns: [
        {
          data: "name",
          title: "Name",
          type: "string",
          sortable: false
        },
        {
          data: "value",
          title: "Value",
          type: "number",
          sortable: false
        }
      ]
    });
  }).fail(function() {
    console.error("error while querying goods");
    throw "goods query error";
  });
  
  $(".new-good").on("click", function(ev) {
    $("#add-good-mp").modal({
      persist: true,
      minHeight: "100px",
      maxHeight: "100px",
      minWidth: "300px"
    });
  });
  
  $("#save-good").on("click", function(ev) {
    window.db.addGood({
      name: $("#name-input").val(),
      value: $("#value-input").val(),
      username: JSON.parse(sessionStorage.getItem("user")).username
    }).done(function(good) {
      $.modal.close();
      window.goodsTable.row.add(good).draw();
      toastr.success("Successfully added good!");
    }).fail(function() {
      
    });
  });
  
  $("#cancel-mp").on("click", function(ev) {
    $.modal.close();
  });
  
})();