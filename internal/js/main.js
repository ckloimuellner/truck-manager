(function() {
  /*var authenticate = function(username, pwd) {
    window.db.findUser(username, pwd).done(function() {
      $.modal.close();
      window.location.href = "internal/index.html?success=true";
      //toastr.success("Login successful!");
    }).fail(function() {
      toastr.error("Username and/or password incorrect!");
    });
  };
  
  $("#login-lnk").on("click", function() {
    $("#login-mp").modal();
  });
  
  $("body").on("click", "#login-btn", function(ev) {
    ev.preventDefault();
    authenticate($("#username-input").val(), $("#pwd-input").val());
  });*/
  
  var sUser = sessionStorage.getItem("user");
  if(sUser) {
  
    $(".username").html("<strong>" + JSON.parse(sUser).username + "</strong>");
    
    if(window.location.search.replace(/.*success=([^&]*)&?.*/gi, "$1") === "true") {
      toastr.success("Login successful!");
    }
    
    $("#logout").on("click", function(ev) {
      sessionStorage.clear();
      window.location.href="../index.html";
    });
  } else {
    window.location.href="../index.html?loginRequired=true";
  }
  
  
})();