(function() {
  
  var user = JSON.parse(sessionStorage.getItem("user"));
  
  window.db.getCustomersForUser(user.username).done(function(customers) {
    window.customerTable = $("#customer-table").DataTable({
      searching: false,
      lengthChange: false,
      data: customers,
      columns: [
        {
          data: "name",
          title: "Name",
          type: "string",
          sortable: false
        },
        {
          data: "location",
          title: "Location",
          type: "string",
          sortable: false
        }
      ]
    });
  }).fail(function() {
    console.error("error while querying customers");
    throw "customer query error";
  });
  
  $(".new-customer").on("click", function(ev) {
    $("#add-customer-mp").modal({
      persist: true,
      onShow: function(dialog) {
        if(!window.gmap) {
          window.createGmap();
        }
      },
      minHeight: "500px",
      minWidth: "300px"
    });
  });
  
  $("#save-customer").on("click", function(ev) {
    window.db.addCustomer({
      name: $("#name-input").val(),
      location: $("#location-input").val(),
      lat: parseFloat($("#location-input").attr("data-lat"), 10),
      lng: parseFloat($("#location-input").attr("data-lng"), 10),
      username: JSON.parse(sessionStorage.getItem("user")).username
    }).done(function(customer) {
      $.modal.close();
      window.customerTable.row.add(customer).draw();
      toastr.success("Successfully added customer!");
    }).fail(function() {
      
    });
  });
  
  // Google Maps Places
  window.gmap = null;
  window.createGmap = function() {
    var markers = [];
    window.gmap = new google.maps.Map(document.getElementById('map-canvas'), {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 10
    });
    
    
    var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(48.108659, 15.539328),
                                                     new google.maps.LatLng(48.308659, 15.739328));
    window.gmap.fitBounds(defaultBounds);
    
    // Create the search box and link it to the UI element.
    var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
    window.gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
    var searchBox = new google.maps.places.SearchBox(
      /** @type {HTMLInputElement} */(input));
    
    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces(), place = places[0];
      
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }
      
      // For each place, get the icon, place name, and location.
      markers = [];
      
      //for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
                                  origin: new google.maps.Point(0, 0),
                                  anchor: new google.maps.Point(17, 34),
                                  scaledSize: new google.maps.Size(25, 25)
        };
        
        console.log(place.geometry.location.lng(), place.geometry.location.lat())
      
        $("#location-input").val(place.formatted_address)
            .attr("data-lat", place.geometry.location.lat())
            .attr("data-lng", place.geometry.location.lng());
        
        console.log("place", place);
        
        // Create a marker for each place.
        var marker = new google.maps.Marker({
          map: window.gmap,
          icon: image,
          draggable: true,
          title: place.name,
          position: place.geometry.location
        });
        
        markers.push(marker);
        
        google.maps.event.addListener(marker, 'dragend', function(event) {
          console.log(event);
          console.debug('final position is '+event.latLng.lat()+' / '+event.latLng.lng());
          
          $("#location-input").val(event.latLng.lat() + ";" + event.latLng.lng())
            .attr("data-lat", event.latLng.lat())
            .attr("data-lng", event.latLng.lng());
        });
        
        var bounds = new google.maps.LatLngBounds(
          new google.maps.LatLng(place.geometry.location.lat() - 0.05, place.geometry.location.lng() - 0.05),
                                                  new google.maps.LatLng(place.geometry.location.lat() + 0.05, place.geometry.location.lng() + 0.05)
        );
        //bounds.extend(place.geometry.location);
      //}
      
      window.gmap.fitBounds(bounds);
    });
    // [END region_getplaces]
    
    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(window.gmap, 'bounds_changed', function() {
      var bounds = window.gmap.getBounds();
      searchBox.setBounds(bounds);
    });
  };
  
  $("#cancel-mp").on("click", function(ev) {
    $.modal.close();
  });
  
  //$("#add-customer-mp").hide();
})();