(function() {
  if(window.location.search.replace(/.*loginRequired=([^&]*)&?.*/gi, "$1") === "true") {
    toastr.error("Before accessing the internal area, you have to login with your user credentials.");
  }
  
  var authenticate = function(username, pwd) {
    window.db.findUser(username, pwd).done(function(user) {
      $.modal.close();
      sessionStorage.setItem("user", JSON.stringify(user));
      window.location.href = "internal/index.html?success=true";
      //toastr.success("Login successful!");
    }).fail(function() {
      toastr.error("Username and/or password incorrect!");
    });
  };
  
  $("#login-lnk").on("click", function() {
    $("#login-mp").modal();
  });
  
  $("body").on("click", "#login-btn", function(ev) {
    ev.preventDefault();
    authenticate($("#username-input").val(), $("#pwd-input").val());
  });
})();